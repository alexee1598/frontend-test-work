import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IResponse, SearchData, TaskResult} from '../../../core/models';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
   private readonly baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  getDateForSearch(): Observable<SearchData> {
    return this.http.get<SearchData>(this.baseUrl);
  }

  sendSearchRequest(data: any): Observable<IResponse> {
    return this.http.post<IResponse>(this.baseUrl, data);
  }

  updateSearchResults(data: any): Observable<IResponse> {
    return this.http.post<IResponse>(`${this.baseUrl}results/`, data);
  }

  getSearchResults(): Observable<TaskResult> {
    return this.http.get<TaskResult>(`${this.baseUrl}results/`);
  }
}
