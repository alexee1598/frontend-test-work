export interface IResponse {
  error: string;
  result: number;
}
