export class SearchResult {
  title: string;
  description: string;
  url: string;
}

export class Task {
  isOpenPanel: boolean;
  id: string;
  status: string;
  created: Date;
  keyword: string;
  completed: Date;
  search_result: SearchResult[];
}

export class TaskResult {
  tasks: Task[];
  response: number;
}
