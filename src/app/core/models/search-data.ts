export class Location {
  location_code: number;
  language_code: string;
  location_name: string;
}

export class Engine {
  name: string;
}

export class SearchData {
  locations: Location[];
  engines: Engine[];
}
