import {Component, OnInit} from '@angular/core';
import {ApiService} from '../shared/services/api/api.service';
import {SearchData} from '../core/models';
import {NgxSpinnerService} from 'ngx-spinner';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  searchData: SearchData;
  searchForm: FormGroup;

  constructor(private apiService: ApiService,
              private spinner: NgxSpinnerService,
              private fb: FormBuilder) {
  }

  trackByFn(index: number): number {
    return index;
  }

  ngOnInit(): void {
    this.spinner.show();
    this.searchForm = this.fb.group({
      engine: new FormControl('', Validators.required),
      request: new FormControl('', Validators.required),
      location: new FormControl('', Validators.required),
    });

    this.apiService.getDateForSearch().subscribe(data => {
      this.searchData = data;
      this.populateFields();
      this.spinner.hide();
    });
  }

  search(): void {
    if (this.searchForm.valid) {
      this.spinner.show();
      this.apiService.sendSearchRequest(this.searchForm.value).subscribe(data => {
        if (data.result === 1) {
          this.searchForm.get('request').setValue('');
        }
        this.spinner.hide();
      });
    }
  }

  private populateFields(): void {
    this.searchForm.get('engine').setValue(this.searchData.engines[0]);
    this.searchForm.get('location').setValue(this.searchData.locations[0]);
  }
}
