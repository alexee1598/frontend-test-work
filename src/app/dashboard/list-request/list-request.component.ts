import {Component, OnInit} from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {Task} from '../../core/models';
import {ApiService} from '../../shared/services/api/api.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './list-request.component.html',
  styleUrls: ['./list-request.component.css']
})
export class ListRequestComponent implements OnInit {
  taskResults: Task[];

  constructor(private apiService: ApiService,
              private spinner: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.spinner.show();
    this.apiService.getSearchResults().subscribe(results => {
        this.taskResults = results.tasks;
        this.spinner.hide();
      }
    );
  }

  trackByFn(index: number): number {
    return index;
  }

  checkNewResults(): void {
    this.spinner.show();
    this.apiService.updateSearchResults('').subscribe(data => {
      if (data.result === 1) {
        this.ngOnInit();
      }
      this.spinner.hide();
    });
  }
}
