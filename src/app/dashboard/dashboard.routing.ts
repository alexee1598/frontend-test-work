import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ListRequestComponent} from './list-request/list-request.component';

const routes: Routes = [
  {
    path: '',
    component: ListRequestComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {
}
