import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListRequestComponent} from './list-request/list-request.component';
import {DashboardRoutingModule} from './dashboard.routing';
import {NgxSpinnerModule} from 'ngx-spinner';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatButtonModule} from '@angular/material/button';

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    NgxSpinnerModule,
    MatExpansionModule,
    MatButtonModule,
  ],
  declarations: [ListRequestComponent]
})
export class DashboardModule {
}
